<?php

class Controller_Admin extends Controller
{
    function __construct()
    {
        $this->model = new Model_Admin();
        $this-> view = new View();
    }

    function action_index()
    {
        $post = $_POST;
        $data = $this->model->getPosts();
        $user = $this->model->loginAuth($post);
        if (isset($_SESSION['admin'])) {
            header('Location: /admin');
            $this->view->generate('admin_view', 'template_view', $data);
        } elseif (isset($_SESSION['user'])) {
            $this->view->generate('login_view', 'template_view', $user);
        }
        $this->view->generate('admin_view', 'template_view', $data);
    }

    function action_logout()
    {
        session_destroy();
        header('Location:/');
    }


}