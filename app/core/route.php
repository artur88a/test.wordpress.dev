<?php

class Route
{
	static function start()
	{
		// контроллер и действие по умолчанию
		$controller_name = 'post';
		$action_name = 'index';
		
		$routes = explode('/', $_SERVER['REQUEST_URI']);

		// получаем имя контроллера
		if ( !empty($routes[1]) )
		{
			$controller_name = $routes[1];
		}

        // получаем имя экшена
        if ( !empty($routes[2]) )
        {
            $action_name = $routes[2];
        }

        // получаем параметры
        if ( !empty($routes[3]) )
        {
            $vars = str_replace("/$controller_name/$action_name/",'',$_SERVER['REQUEST_URI']);

            $vars = explode('/',$vars);
            $get = '';
            foreach ($vars as $k => $var) {
                if (($k % 2) == 1){
                    $get .= '='.$var;
                }else{
                    $get .= '&'.$var;
                }
            }
            parse_str($get, $output);
            foreach ($output as $k => $v){
                $_GET[$k] = $v;
            }
        }

		// добавляем префиксы
		$controller_name = 'Controller_'.$controller_name;
		$action_name = 'action_'.$action_name;

//        echo "Controller: $controller_name <br>";
//        echo "Action: $action_name <br>";

		// подцепляем файл с классом модели (файла модели может и не быть)

        spl_autoload_register(function ($name) {
            $model_file = strtolower($name).'.php';
            $model_path = APP ."models/".$model_file;
            if(file_exists($model_path))
            {
                include APP. "models/".$model_file;
            }
        });

		// подцепляем файл с классом контроллера
		$controller_file = strtolower($controller_name).'.php';
		$controller_path = APP. "controllers/".$controller_file;
		if(file_exists($controller_path))
		{
			include APP. "controllers/".$controller_file;
		}
		else
		{
			/*
			правильно было бы кинуть здесь исключение,
			но для упрощения сразу сделаем редирект на страницу 404
			*/
			Route::ErrorPage404();
		}
		
		// создаем контроллер
		$controller = new $controller_name;
		$action = $action_name;
		
		if(method_exists($controller, $action))
		{
			// вызываем действие контроллера
			$controller->$action();
		}
		else
		{
			// здесь также разумнее было бы кинуть исключение
			Route::ErrorPage404();
		}
	
	}

	function ErrorPage404()
	{
	    echo 'Нет такого пути!';die;
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
		header("Status: 404 Not Found");
		header('Location:'.$host.'404');
    }
    
}