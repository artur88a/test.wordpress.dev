<div id="postlist">
    <?php foreach ($data['post'] as $row):?>
        <div class="well">
            <div class="media">
                <a class="media-body" href="/post/view/id/<?php echo $row['post_id']?>" title="<?php echo $row['title'];?>" >
                    <h4 class="media-heading"><?php echo $row['title'];?></h4>
                </a>
                <p class="text-right">Автор: <?php echo $row['author'];?></p>
                <p><?php echo $row['content'];?></p>
                <ul class="list-inline list-unstyled item-footer postFooter">
                    <li><span><i class="glyphicon glyphicon-calendar"></i><?php echo $row['date'];?></span></li>
                    <li>|</li>
                    <span><i class="glyphicon glyphicon-comment"></i> 2 comments</span>
                    <li>|</li>
                    <li>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star-empty"></span>
                    </li>
                </ul>
            </div>
        </div>
    <?php endforeach; ?>
</div>
