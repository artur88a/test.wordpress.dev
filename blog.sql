/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : blog

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2017-02-20 18:10:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `status` int(11) DEFAULT '0',
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) DEFAULT NULL,
  `category_desc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('0', '1', 'sport', 'category 1 category 1 category 1 category 1 category 1 ');
INSERT INTO `category` VALUES ('0', '2', 'news', 'category 2 category 2 category 2 category 2 category 2 ');
INSERT INTO `category` VALUES ('0', '3', 'afisha', 'category 3 category 2 category 2 category 2 category 2 ');

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `date` time NOT NULL,
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of comments
-- ----------------------------
INSERT INTO `comments` VALUES ('1', '2', 'Arthur', 'Agratina', 'asdasdasdads', '00:20:17');
INSERT INTO `comments` VALUES ('2', '3', 'Arthur', 'Agratina', 'asdasd', '00:00:00');

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `category_id` int(11) DEFAULT NULL,
  `images` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`post_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES ('1', 'News 1', 'News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1', '2', '', 'admin', '2017-02-12 09:25:04');
INSERT INTO `posts` VALUES ('2', 'News 2', 'News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 ', '1', null, 'admin', '2017-02-12 09:25:12');
INSERT INTO `posts` VALUES ('3', 'News 3', 'News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 ', '3', null, 'admin', '2017-02-12 09:25:23');
INSERT INTO `posts` VALUES ('4', 'News 4', 'News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1', '2', '', 'admin', '2017-02-17 23:53:53');
INSERT INTO `posts` VALUES ('5', 'News 5', 'News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1', '2', '', 'admin', '2017-02-13 00:21:21');
INSERT INTO `posts` VALUES ('6', 'News 6', 'News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1', '2', '', 'admin', '2017-02-12 09:25:04');
INSERT INTO `posts` VALUES ('7', 'News 7', 'News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1', '2', '', 'admin', '2017-02-12 09:25:04');
INSERT INTO `posts` VALUES ('8', 'News 8', 'News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 News 2 ', '1', '', 'admin', '2017-02-12 09:25:12');
INSERT INTO `posts` VALUES ('9', 'News 9', 'News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 News 3 ', '3', '', 'admin', '2017-02-12 09:25:23');
INSERT INTO `posts` VALUES ('10', 'News 10', 'News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1', '2', '', 'admin', '2017-02-13 00:21:18');
INSERT INTO `posts` VALUES ('11', 'News 11', 'News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1', '2', '', 'admin', '2017-02-13 00:21:21');
INSERT INTO `posts` VALUES ('12', 'News 12', 'News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1', '2', '', 'admin', '2017-02-12 09:25:04');
INSERT INTO `posts` VALUES ('13', 'News 13', 'News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1', '2', '', 'admin', '2017-02-12 09:25:04');
INSERT INTO `posts` VALUES ('14', 'News 14', 'News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1News 1', '2', '', 'admin', '2017-02-12 09:25:04');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Amazzon', null, '244767ee6ed7d2fd0271bd41f9943d5f', 'agratinaa@gmail.com', 'admin');
