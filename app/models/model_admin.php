<?php
class Model_Admin extends Model
{
    function clean($value = "")
    {
        $value = trim($value);
        $value = stripslashes($value);
        $value = strip_tags($value);
        $value = htmlspecialchars($value);

        return $value;
    }

    function loginAuth($post)
    {
        if ($_POST) {
            if (isset($post['login']) && isset($post['password'])) {

                $login = $this->clean($post['login']);
                $pass = $this->clean($post['password']);
                $password = md5($pass);

                $userParam = array();
                try {
                    $db = $this->getConnection();
                    $select = $db->query("SELECT * FROM users WHERE email='$login'");
                    $row = $select->fetch();

                    if ($row != false) {
                        if ($password == $row['password']) {
                            if ($row['role'] = 'admin'){
                                $_SESSION['admin'] = $row['password'];
                                $user = 'Спасибо! Вы вошли как Админ!';
                            }else{
                                $_SESSION['user'] = $row['password'];
                                $user = 'Спасибо! Вы вошли как Пользователь!';
                            }
                        } else {
                            $user = 'Вы ввели не правельный пароль!';
                        }
                    } else {
                        $user = 'Пользователя с таким email нету!';
                    }
                } catch (PDOException $e) {
                    echo 'Подключение не удалось: ' . $e->getMessage();
                }

            }
        }
        return $user;
    }


    function getPosts()
    {
        $db = $this->getConnection();

        $posts = array();
        try {
            $select = $db->query("SELECT posts.post_id, posts.title, posts.content, posts.category_id, posts.images,
                                  posts.author, posts.date, category.category_id, category.category_name, category.category_desc
                                  FROM posts INNER JOIN category ON posts.category_id = category.category_id
                                  ORDER BY posts.date DESC ");
            $i = 0;
            while ($row = $select->fetch()) {
                $posts[$i]['post_id'] = $row['post_id'];
                $posts[$i]['title'] = $row['title'];
                $posts[$i]['date'] = $row['date'];
                $posts[$i]['images'] = $row['images'];
                $posts[$i]['category_name'] = $row['category_name'];
                $i++;
            }

        } catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
        }

        return $posts;
    }
    function getPost()
    {
        $db = $this->getConnection();

        $posts = array();
        try {
            $select = $db->query("SELECT posts.post_id, posts.title, posts.content, posts.category_id, posts.images,
                                  posts.author, posts.date, category.category_id, category.category_name, category.category_desc
                                  FROM posts INNER JOIN category ON posts.category_id = category.category_id
                                  ORDER BY posts.date DESC ");
            $i = 0;
            while ($row = $select->fetch()) {
                $posts[$i]['post_id'] = $row['post_id'];
                $posts[$i]['title'] = $row['title'];
                $posts[$i]['date'] = $row['date'];
                $posts[$i]['images'] = $row['images'];
                $posts[$i]['category_name'] = $row['category_name'];
                $i++;
            }

        } catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
        }

        return $posts;
    }
}