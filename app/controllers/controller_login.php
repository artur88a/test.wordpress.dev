<?php

class Controller_Login extends Controller
{
    function __construct()
    {
        $this->model = new Model_Login();
        $this->view = new View();
    }

    function action_index()
    {
        $post = $_POST;
        $user = $this->model->loginAuth($post);

        if (isset($_SESSION['admin'])) {
            header('Location: /admin');
            $this->view->generate('admin_view', 'template_view', $user);
        } elseif (isset($_SESSION['user'])) {
            $this->view->generate('login_view', 'template_view', $user);
        }
    }
    function action_logout()
    {
        $logout = $this->logout();
        $this->view->generate('index_view', 'template_view');

    }
}