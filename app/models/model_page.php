<?php

class Model_Page extends Model
{
    const SHOW_BY_DEFAULT = 10;
	public function getPage($pageID)
	{
        $db = $this->getConnection();

        $postList = array();
        try {

            $limit1 = 0;
            $postRow = $db->query('SELECT * FROM posts');
            $total = count($postRow->fetch());
            $page = ceil($total / self::SHOW_BY_DEFAULT); // кол-во страниц


        } catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
        }
        return $page;
	}

    public function getPost($postID)
    {
        $db = $this->getConnection();

        $postList = array();
        try {
            $select = $db->query('SELECT title, content FROM posts WHERE post_id ='. $postID);
            $i = 0;
            while ($row = $select->fetch())
            {
                $postList[$i]['title'] = $row['title'];
                $postList[$i]['content'] = $row['content'];
                $i++;
            }
        } catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
        }
        return $postList;

    }

}
