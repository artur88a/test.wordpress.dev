<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->

    <link href="/template/bootstrap/css/bootstrap.css" rel="stylesheet">
<!--    <link href="/template/bootstrap/css/bootstrap-theme.css" rel="stylesheet">-->
    <style type="text/css">
        body {
            padding-top: 100px;
            padding-bottom: 40px;
        }
    </style>
    <link href="/template/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="/template/bootstrap/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/template/bootstrap/css/style.css"/>



    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="brand" href="#">My Site </a>
                <div class="nav-collapse collapse">
                    <ul class="nav">
                        <li><a href="/">Home</a></li>
                    </ul>
                    <?php if ($_SESSION['admin']):?>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#"><i class="glyphicon glyphicon-user"></i> Admin <span class="caret"></span></a>
                            <ul id="g-account-menu" class="dropdown-menu" role="menu">
                                <li><a href="#">My Profile</a></li>
                            </ul>
                            <ul id="g-account-menu" class="dropdown-menu" role="menu">
                                <li><a href="/admin">Admin Panel</a></li>
                            </ul>
                        </li>
                        <li><a href="logout"><i class="glyphicon glyphicon-lock"></i> Logout</a></li>
                    </ul>
                    <?php elseif ($_SESSION['user']):?>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#"><i class="glyphicon glyphicon-user"></i> Admin <span class="caret"></span></a>
                                <ul id="g-account-menu" class="dropdown-menu" role="menu">
                                    <li><a href="#">My Profile</a></li>
                                </ul>
                            </li>
                            <li><a href="login/logout"><i class="glyphicon glyphicon-lock"></i> Logout</a></li>
                        </ul>
                    <?php endif; ?>
                    <?php if (! $_SESSION): ?>
                        <form class="navbar-form pull-right" id="login" method="POST" action="">
                            <input class="span2" name="login" type="login" placeholder="Email">
                            <input class="span2" name="password" type="password" placeholder="Password">
                            <button type="submit" name="dologin" class="btn btn-success">Sign in</button>
                        </form>
                    <?php endif; ?>
                </div><!--/.nav-collapse -->
            </div>
        </div>
    </div>
<div class="container">