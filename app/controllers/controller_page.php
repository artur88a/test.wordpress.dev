<?php

class Controller_Page extends Controller
{

	function __construct()
	{
		$this->model = new Model_Page();
		$this->view = new View();
	}
	
	function action_index()
	{
        $pageID = (int)$_GET['page']; //номер страницы
		$data = $this->model->getAllPage($pageID);
		$this->view->generate('pagination_view', 'template_view', $data);
	}
    function action_view()
    {
        if (($_GET['id']) === null){
            die('Page not found');
        }else{
            $pageID = (int)$_GET['id'];
        }

        $data = $this->model->getPage($pageID);
        $this->view->generate('page_view', 'template_view', $data);
    }
}
