<div id="loginModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3>Have an Account?</h3>
    </div>
    <div class="modal-body">
        <div class="well">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#logins" data-toggle="tab">Login</a></li>
                <li><a href="#create" data-toggle="tab">Create Account</a></li>

            </ul>
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane active in" id="logins">
                    <form id="tab">
                        <label>E-mail:</label>
                        <input type="email" name="email" value="" class="input-xlarge" placeholder="example@gmail.com">
                        <label>Password:</label>
                        <input type="password" name="password" value="" class="input-xlarge" placeholder="*******">
                        <div>
                            <button class="btn btn-primary">Login</button>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="create">
                    <form id="tab">
                        <label>First Names</label>
                        <input type="text" value="" name="name" class="input-xlarge" placeholder="Your Name">
                        <label>Last Names</label>
                        <input type="text" value="" name="lastname" class="input-xlarge" placeholder="Your LastName">
                        <label>Emails</label>
                        <input type="text" value="" name="email" class="input-xlarge" placeholder="example@gmail.com">
                        <label>Password:</label>
                        <input type="password" name="password" value="" class="input-xlarge" placeholder="*******">
                        <div>
                            <button class="btn btn-primary">Create Account</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    <div class="mystyle"></div>
<?php //extract($data); ?>
<?php //if($login_status=="access_granted") { ?>
<!--<p style="color:green">Авторизация прошла успешно.</p>-->
<?php //} elseif($login_status=="access_denied") { ?>
<!--<p style="color:red">Логин и/или пароль введены неверно.</p>-->
<?php //} ?>

