<?php foreach ($data['post'] as $item):?>
<div class="col-md-12">
        <h2><?php echo $item['title']; ?></h2>
        <p class="lead"><i class="fa fa-user"></i> by <?php echo $item['author']; ?>
        </p>
        <hr>
        <p><i class="fa fa-tags"></i> Категорія: <a href="/category/view/id/<?php echo $item['category_id'] ?>"><span class="badge badge-info"><?php echo $item['category_name']; ?></span></a>  <i class="fa fa-calendar"></i> Опубліковано в <?php echo $item['date']; ?></p>
        <?php if (!is_null($item['images'])):?>
            <hr>
                <img src="<?php echo $item['images']; ?>" class="img-responsive">
        <?php endif;?>
        <p class="lead"><?php echo $item['content']; ?></p>
       <br/>

        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

        <div class="g-plusone" data-annotation="inline" data-width="300" data-href=""></div>

        <!-- Helyezd el ezt a címkét az utolsó +1 gomb címke mögé. -->
        <script type="text/javascript">
            (function() {
                var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                po.src = 'https://apis.google.com/js/platform.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
            })();
        </script>
        <br/>
        <hr>
<?php endforeach; ?>
<!-- the comment box -->
<div class="well">
    <h4><i class="fa fa-paper-plane-o"></i> Додати коментар:</h4>
    <form role="form">
        <div class="form-group">
            <textarea class="form-control" rows="5"></textarea>
        </div>
        <button type="submit" class="btn btn-primary"><i class="fa fa-reply"></i> Відправити</button>
    </form>
</div>
<script src="https://apis.google.com/js/plusone.js">
</script>
<hr>

<!-- the comments -->
<h4><i class="fa fa-comment"></i> Користувач - Сказав:
    <small> 9:41 PM on August 24, 2014</small>
</h4>
<p>Excellent post! Thank You the great article, it was useful!</p>

<h4><i class="fa fa-comment"></i> Користувач - Сказав:
    <small> 9:47 PM on August 24, 2014</small>
</h4>
<p>Excellent post! Thank You the great article, it was useful!</p>
</div>