<?php include 'layouts/header.php';?>
        <div class="row-fluid">
            <?php if ($_SERVER['REQUEST_URI'] != '/login' && $_SERVER['REQUEST_URI'] != '/admin'): ?>
            <div class="span3">
                <div class="well sidebar-nav">
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                        <li><a tabindex="-1" href="#">Regular link</a></li>
                        <li class="disabled"><a tabindex="-1" href="#">Disabled link</a></li>
                        <li><a tabindex="-1" href="#">Another link</a></li>
                    </ul>
                    <ul class="nav nav-list">
                        <li class="nav-header">Категорії: </li>
                            <?php foreach ($data['categories'] as $row): ?>
                                <li><a tabindex="-1" href="/category/view/id/<?php echo $row['category_id'] ?>"><?php echo $row['category_name'];?></a></li>
                            <?php endforeach; ?>
                    </ul>
                </div><!--/.well -->
            </div><!--/span-->
            <?php endif; ?>
            <div class="span9">
                <?php if ($_SERVER['REQUEST_URI'] == '/'): ?>
                    <div class="hero-unit">
                        <h1>Hello, world!</h1>
                        <p>This is a template for a simple marketing or informational website. It includes a large callout called the hero unit and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
                    </div>
                <?php endif; ?>
                    <?php include 'app/views/'.$content_view . '.php'; ?>
                <div class="span9" style="text-align: center">
                    <?php if ($_SERVER['REQUEST_URI'] == '/'): ?>
                        <div class="pagination">
                            <ul>
                                <li><a href="#">Prev</a></li>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">Next</a></li>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>
            </div><!--/span-->
        </div><!--/row-->
<?php include 'layouts/footer.php'; ?>