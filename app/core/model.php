<?php

class Model
{

	/*
		Модель обычно включает методы выборки данных, это могут быть:
			> методы нативных библиотек pgsql или mysql;
			> методы библиотек, реализующих абстракицю данных. Например, методы библиотеки PEAR MDB2;
			> методы ORM;
			> методы для работы с NoSQL;
			> и др.
	*/

	// метод выборки данных
	public function get_data()
	{
//        //include the class
//        include('paginator.php');
//
//        $params = include 'db_config.php';
//
//        $dbp = "mysql:host={$params['dbhost']};dbname={$params['dbname']};";
//        $db = new PDO($dbp, $params['dbuser'], $params['dbpass']);
//
//        //create new object pass in number of pages and identifier
//        $pages = new Paginator('10','p');
//
//        //get number of total records
//        $rows = $db->query('SELECT id FROM posts');
//        $total = count($rows);
//
//        //pass number of records to
//        $pages->set_total($total);
//
//        $data = $db->query('SELECT * FROM posts '.$pages->get_limit());
//        $arr= array();
//        foreach($data as $row) {
//            $arr [] = array(
//                'post_id' => $row['post_id'],
//                'title' => $row['title'],
//                'content' => $row['content'],
//                'category_id' => $row['category_id'],
//                'author' => $row['author'],
//                'date' => $row['date'],
//            );
//        }
//
//        //create the page links
////        echo $pages->page_links();
//        return $arr;
	}
    function clean($value = "")
    {
        $value = trim($value);
        $value = stripslashes($value);
        $value = strip_tags($value);
        $value = htmlspecialchars($value);

        return $value;
    }

    function loginAuth($post)
    {
        if ($_POST) {
            if (isset($post['login']) && isset($post['password'])) {

                $login = $this->clean($post['login']);
                $pass = $this->clean($post['password']);
                $password = md5($pass);

                $userParam = array();
                try {
                    $db = $this->getConnection();
                    $select = $db->query("SELECT * FROM users WHERE email='$login'");
                    $row = $select->fetch();

                    if ($row != false) {
                        if ($password == $row['password']) {
                            if ($row['role'] = 'admin'){
                                $_SESSION['admin'] = $row['password'];
                                $user = 'Спасибо! Вы вошли как Админ!';
                            }else{
                                $_SESSION['user'] = $row['password'];
                                $user = 'Спасибо! Вы вошли как Пользователь!';
                            }
                        } else {
                            $user = 'Вы ввели не правельный пароль!';
                        }
                    } else {
                        $user = 'Пользователя с таким email нету!';
                    }
                } catch (PDOException $e) {
                    echo 'Подключение не удалось: ' . $e->getMessage();
                }

            }
        }
        return $user;
    }

    public function getConnection()
    {
        $params = include 'db_config.php';

        $dbp = "mysql:host={$params['dbhost']};dbname={$params['dbname']};";
        $db = new PDO($dbp, $params['dbuser'], $params['dbpass']);

        return $db;
	}
}