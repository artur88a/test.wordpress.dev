<?php

class Controller_Category extends Controller
{
	function __construct()
	{
		$this->model = new Model_Category();
		$this->view = new View();
	}
	function action_index()
	{
        $data = $this->model->getCategoryList();
        $this->view->generate('category_list_view', 'template_view', $data);

	}
    function action_view()
    {
        $categories = $this->model->getCategoryList();
        $categoryId = (int)$_GET['id'];
        $postCategory = $this->model->getPostCategory($categoryId);
        $data = array(
            'postCat' => $postCategory,
            'categories' => $categories,
        );

        $this->view->generate('category_view', 'template_view', $data);
    }
}
