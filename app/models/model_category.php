<?php

class Model_Category extends Model
{
	
	public function getPostCategory($categoryId )
	{
        $db = $this->getConnection();

        $postList = array();
        try {
            $select = $db->query("SELECT category.category_id, posts.post_id, posts.title, posts.content, posts.category_id,
                                  posts.images, posts.author, posts.date FROM posts INNER JOIN category 
                                  ON posts.category_id = category.category_id WHERE posts.category_id = $categoryId ");
            $i = 0;
            while ($row = $select->fetch())
            {
                $postList[$i]['post_id'] = $row['post_id'];
                $postList[$i]['title'] = $row['title'];
                $postList[$i]['content'] = $row['content'];
//                $postList[$i]['post_category'] = $row['post_category'];
                $postList[$i]['images'] = $row['images'];
                $postList[$i]['author'] = $row['author'];
                $postList[$i]['date'] = $row['date'];
                $i++;
            }
        } catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
        }

        return $postList;
	}

	public function getCategoryList()
    {
        $db = $this->getConnection();

        $catList = array();
        try {
            $select = $db->query('SELECT category_id, category_name, category_desc  FROM category ORDER BY category_id ASC');

            $i = 0;
            while ($row = $select->fetch())
            {
                $catList[$i]['category_id'] = $row['category_id'];
                $catList[$i]['category_name'] = $row['category_name'];
                $catList[$i]['category_desc'] = $row['category_desc'];
                $i++;
            }
        } catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
        }
        return $catList;
    }

}
