<?php

class Model_Post extends Model
{
    const SHOW_BY_DEFAULT = 10;

    public function clean($value = "")
    {
        $value = trim($value);
        $value = stripslashes($value);
        $value = strip_tags($value);
        $value = htmlspecialchars($value);

        return $value;
    }
    public function getAllPost($limit = self::SHOW_BY_DEFAULT)
    {

        $db = $this->getConnection();
        $postList = array();
        try {

            $select = $db->query('SELECT title, content, post_id, category_id, images, author, date FROM posts LIMIT ' . $limit);
            $postRow = $db->query('SELECT * FROM posts');
            $total = count($postRow->fetch());
            $page = ceil($total / $limit); // кол-во страниц

            $i = 0;
            while ($row = $select->fetch()) {
                $postList[$i]['post_id'] = $row['post_id'];
                $postList[$i]['title'] = $row['title'];
                $postList[$i]['content'] = $row['content'];
                $postList[$i]['category_id'] = $row['category_id'];
                $postList[$i]['images'] = $row['images'];
                $postList[$i]['author'] = $row['author'];
                $postList[$i]['date'] = $row['date'];
                $i++;
            }
        } catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
        }
        return $postList;
    }

    public function getPost($postID)
    {
        $db = $this->getConnection();

        $postList = array();
        try {
            $select = $db->query('SELECT posts.post_id, posts.title, posts.content, posts.images, posts.category_id, posts.author, posts.date, category.category_name FROM posts 
                                  LEFT JOIN category ON (posts.category_id = category.category_id) WHERE post_id =' . $postID);
            $i = 0;
            while ($row = $select->fetch()) {
                $postList[$i]['post_id'] = $row['post_id'];
                $postList[$i]['title'] = $row['title'];
                $postList[$i]['content'] = $row['content'];
                $postList[$i]['images'] = $row['images'];
                $postList[$i]['category_id'] = $row['category_id'];
                $postList[$i]['category_name'] = $row['category_name'];
                $postList[$i]['author'] = $row['author'];
                $postList[$i]['date'] = $row['date'];
                $i++;
            }
        } catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
        }
        return $postList;

    }

    public function getCategoryList()
    {
        $db = $this->getConnection();

        $catList = array();
        try {
            $select = $db->query('SELECT category_id, category_name, category_desc  FROM category ORDER BY category_id ASC');

            $i = 0;
            while ($row = $select->fetch()) {
                $catList[$i]['category_id'] = $row['category_id'];
                $catList[$i]['category_name'] = $row['category_name'];
                $catList[$i]['category_desc'] = $row['category_desc'];
                $i++;
            }
        } catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
        }
        return $catList;
    }

    public function getPostCategory($categoryId, $limit = self::SHOW_BY_DEFAULT)
    {
        $db = $this->getConnection();

        $postList = array();
        try {
            $select = $db->query("SELECT category.category_id, posts.post_id, posts.title, posts.content, posts.category_id,
                                  posts.images, posts.author, posts.date FROM posts INNER JOIN category 
                                  ON posts.category_id = category.category_id WHERE posts.category_id = $categoryId LIMIT $limit");
            $i = 0;
            while ($row = $select->fetch()) {
                $postList[$i]['post_id'] = $row['post_id'];
                $postList[$i]['title'] = $row['title'];
                $postList[$i]['content'] = $row['content'];
                $postList[$i]['category_id'] = $row['category_id'];
                $postList[$i]['images'] = $row['images'];
                $postList[$i]['author'] = $row['author'];
                $postList[$i]['date'] = $row['date'];
                $i++;
            }

        } catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
        }
        return $postList;
    }

    public function getCategory()
    {
        $db = $this->getConnection();

        $catList = array();
        try {
            $select = $db->query('SELECT category_id, category_name, category_desc  FROM category ORDER BY category_id ASC');

            $i = 0;
            while ($row = $select->fetch()) {
                $catList[$i]['category_id'] = $row['category_id'];
                $catList[$i]['category_name'] = $row['category_name'];
                $catList[$i]['category_desc'] = $row['category_desc'];
                $i++;
            }
        } catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
        }
        return $catList;
    }

    public function loginAuth($formLogin)
    {
            if (isset($formLogin['login']) && isset($formLogin['password'])) {
                $login = $this->clean($formLogin['login']);
                $pass = $this->clean($formLogin['password']);
                $password = md5($pass);

                try {
                    $db = $this->getConnection();
                    $select = $db->query("SELECT * FROM users WHERE email='$login'");
                    $row = $select->fetch();

                    if ($row != false) {
                        if ($password == $row['password']) {
                            if ($row['role'] = 'admin'){
                                $_SESSION['admin'] = $row['password'];
                                $user = 'Спасибо! Вы вошли как Админ!';
                            }else{
                                $_SESSION['user'] = $row['password'];
                                $user = 'Спасибо! Вы вошли как Пользователь!';
                            }
                        } else {
                            $user = 'Вы ввели не правельный пароль!';
                        }
                    } else {
                        $user = 'Пользователя с таким email нету!';
                    }
                } catch (PDOException $e) {
                    echo 'Подключение не удалось: ' . $e->getMessage();
                }

            }
        return $user;
    }
}