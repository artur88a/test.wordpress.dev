<hr>
<footer>
    <p>Агратина Артур &copy; Company 2017</p>
</footer>
</div>
<script type="text/javascript">
    jQuery(".alert").addClass("in").fadeOut(4500);

    /* swap open/close side menu icons */
    jQuery('[data-toggle=collapse]').click(function(){
        // toggle icon
        jQuery(this).find("i").toggleClass("glyphicon-chevron-right glyphicon-chevron-down");
    });
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/template/bootstrap/js/jquery-3.1.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/template/bootstrap/js/bootstrap.js"></script>
</body>
</html>