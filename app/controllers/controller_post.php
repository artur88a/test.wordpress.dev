<?php

class Controller_Post extends Controller
{

	function __construct()
	{
		$this->model = new Model_Post();
		$this->view = new View();
	}
	
	function action_index()
	{
	    if (isset($_POST)){
	        $formLogin = $_POST;
        }else{
	        $formLogin = null;
        }
	    $categories = $this->model->getCategoryList();
        $post = $this->model->getAllPost();
        $login = $this->model->loginAuth($formLogin);
	    $data = array(
	        'post' => $post,
	        'categories' => $categories,
        );

	    $this->view->generate('index_view', 'template_view', $data);
	}
    function action_view()
    {
        if (isset($_GET['id']))
        {
            $postID = $_GET['id'];
        }
        $post = $this->model->getPost($postID);

        $categories = $this->model->getCategoryList();
        $postCategory = $this->model->getCategory($postID);

        $data = array(
            'post' => $post,
            'postCategory' => $postCategory,
            'categories'   => $categories,
        );
        $this->view->generate('post_view', 'template_view', $data);
    }
}
